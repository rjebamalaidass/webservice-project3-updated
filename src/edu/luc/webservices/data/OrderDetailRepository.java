package edu.luc.webservices.data;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.luc.webservices.domain.OrderDetail;
import edu.luc.webservices.domain.OrderDetailIDClass;

public interface OrderDetailRepository extends JpaRepository<OrderDetail, OrderDetailIDClass> {
	List<OrderDetail> findByOrderid(Integer orderId);

	void deleteByOrderid(int orderid);
}
