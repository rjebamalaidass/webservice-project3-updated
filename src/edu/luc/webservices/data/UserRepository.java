package edu.luc.webservices.data;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.luc.webservices.domain.User;

public interface UserRepository extends JpaRepository<User, Integer> {

}
