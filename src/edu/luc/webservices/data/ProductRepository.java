package edu.luc.webservices.data;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import edu.luc.webservices.domain.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {
	
	public List<Product> findByCategory(String category);
	public List<Product> findByProductnameContains(String partialname);
	@Query("SELECT DISTINCT a.category FROM Product a")
    List<String> findDistinctCategory();
	
	

}
