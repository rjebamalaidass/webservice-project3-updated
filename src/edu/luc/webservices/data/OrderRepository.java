package edu.luc.webservices.data;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.luc.webservices.domain.Order;

public interface OrderRepository extends JpaRepository<Order, Integer>{

	List<Order> findByUserid(int userid);

	List<Order> findByUseridAndOrderstatus(int userid, String status);

	


}
