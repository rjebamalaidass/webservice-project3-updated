package edu.luc.webservices.data;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.luc.webservices.domain.Partner;

public interface PartnerRepository extends JpaRepository<Partner, Integer> {

}
