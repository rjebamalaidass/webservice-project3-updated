package edu.luc.webservices.data;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.luc.webservices.domain.PartnerOrder;

public interface PartnerOrderRepository extends JpaRepository<PartnerOrder, Integer>{

	List<PartnerOrder> findByPartnerid(int partnerid);

}
