package edu.luc.webservices.activity;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.luc.webservices.bean.Link;
import edu.luc.webservices.bean.ProductBean;
import edu.luc.webservices.domain.Product;
import edu.luc.webservices.services.ProductService;

@Service
public class ProductActivity {
	
	@Autowired
	ProductService productService;
	
	
	
	private ProductBean mapBeanValues(Product source)
	{
		ProductBean dest = new ProductBean();
		dest.setProductid(source.getProductid());
		dest.setProductname(source.getProductname());
		dest.setDescription(source.getDescription());
		dest.setCategory(source.getCategory());
		dest.setAvailablequantity(source.getAvailablequantity());
		dest.setPrice(source.getPrice());
		
		List<Link> linkList = new ArrayList<Link>();
		
		Link productDetailLink = new Link();
		productDetailLink.setAction("detail");
		productDetailLink.setUrl("http://localhost:8081/product/productdetails/"+source.getProductid());
		linkList.add(productDetailLink);
		
		Link orderLink = new Link();
		orderLink.setAction("order");
		orderLink.setUrl("http://localhost:8082/order/placeorder/<%=USER_ID%>");
		linkList.add(orderLink);
		
		dest.setLinks(linkList);
	
		
		return dest;
	}
	
	

	public List<ProductBean> getAllProducts() {
		List<Product> productList = productService.getAllProducts();
		List<ProductBean> productBeanList = new ArrayList<ProductBean>();
		
		productList.stream().forEach( p ->
		{
			
			productBeanList.add(mapBeanValues(p));
		}
				);
		
		return productBeanList;
		
	}

	public ProductBean getProductById(int productId) {
		Product p = productService.getProductById(productId);
		return mapBeanValues(p);
		
	}

	public List<String> getAllProductCategories() {
		return productService.getAllProductCategories();
	}

	public List<ProductBean> getProductByCategory(String a) {
		List<Product> productList = productService.getProductByCategory(a);
		List<ProductBean> productBeanList = new ArrayList<ProductBean>();
		
		productList.stream().forEach( p ->
		{
			productBeanList.add(mapBeanValues(p));
		}
				);
		
		return productBeanList;
		
	}

	public ProductBean insertProduct(String productname, String description, String category, int partnerid) {
		int productid = productService.insertProduct(productname, description, category, partnerid);
		Product p = productService.getProductById(productid);
		return mapBeanValues(p);
	}



	public ProductBean updateProductDetail(ProductBean product) {
		int productId = product.getProductid();
		productService.updateProductDetail(product);
		Product p = productService.getProductById(productId);
		return mapBeanValues(p);
	}

}
