package edu.luc.webservices.activity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.luc.webservices.bean.Link;
import edu.luc.webservices.bean.Payment;
import edu.luc.webservices.bean.ProductOrder;
import edu.luc.webservices.bean.ShippingStatus;
import edu.luc.webservices.bean.WebAddress;
import edu.luc.webservices.bean.WebOrder;
import edu.luc.webservices.domain.Order;
import edu.luc.webservices.domain.OrderDetail;
import edu.luc.webservices.services.AddressService;
import edu.luc.webservices.services.OrderDetailService;
import edu.luc.webservices.services.OrderService;

@Service
public class OrderActivity {

	@Autowired
	OrderService orderService;
	
	@Autowired
	OrderDetailService orderDetailService;
	
	@Autowired
	AddressService addressService;
	
	public WebOrder MapWebOrderBean(Order order)
	{
		WebAddress address = new WebAddress();
		address.setAddress1(order.getAddress().getAddress1());
		address.setAddress2(order.getAddress().getAddress2());
		address.setAddressid(order.getAddress().getAddressid());
		address.setAddresstype(order.getAddress().getAddresstype());
		address.setCity(order.getAddress().getCity());
		address.setState(order.getAddress().getState());
		address.setZip(order.getAddress().getZip());
		address.setUserid(order.getUserid());
		
		List<OrderDetail> orderDetailList = orderDetailService.findByOrderId(order.getOrderid());
	
		List<ProductOrder> productList = setupProductDetailLink(orderDetailList);
		
		WebOrder wo = new WebOrder();
		wo.setAddress(address);
		wo.setProductList(productList);
		wo.setOrderdate(order.getOrderdate());
		wo.setOrderId(order.getOrderid());
		wo.setOrderprice(order.getOrderprice());
		wo.setOrderstatus(order.getOrderstatus());
		wo.setShippingcost(order.getShippingcost());
		wo.setTotalprice(order.getTotalprice());
		
		wo.setLinks(setupLink(order));
		
		return wo;
	}

	
	
	public List<Link> setupLink(Order order)
	{
		List<Link> linkList = new ArrayList<Link>();
		
		Link orderDetailLink = new Link();
		orderDetailLink.setAction("detail");
		orderDetailLink.setUrl("http://localhost:8082/order/orderdetail/"+order.getOrderid());
		linkList.add(orderDetailLink);
		
		/**
		 * We pass the payment link only if the payment is not made.
		 * In the order table, if the payment status is success that means the user had already paid
		 * and we do not need to send the payment link
		 */
		if(!StringUtils.equalsIgnoreCase(order.getPaymentstatus(), "success"))
		{
		
			Link paymentLink = new Link();
			paymentLink.setAction("payment");
			paymentLink.setUrl("http://localhost:8082/order/paymentDetail/"+order.getOrderid());
			linkList.add(paymentLink);
			}
		
		return linkList;
	}
	
	public List<ProductOrder> setupProductDetailLink(List<OrderDetail> detailList)
	{
		List<ProductOrder> productList = new ArrayList<ProductOrder>();
		for(OrderDetail o : detailList)
		{
			ProductOrder pr = new ProductOrder();
			pr.setPrice(o.getTotalprice());
			pr.setProductId(o.getProductid());
			pr.setQuantity(o.getQuantity());
			List<Link> linkList = new ArrayList<Link>();
			Link productDetailLink = new Link();
			productDetailLink.setAction("detail");
			productDetailLink.setUrl("http://localhost:8081/product/productdetails/"+o.getProductid());
			linkList.add(productDetailLink);
			pr.setLinks(linkList);
			
			productList.add(pr);
		}
			return productList;
	}
	
	
	
	public WebOrder placeOrder(int userid, WebOrder order) throws Exception {
		int address = addressService.insertAddress(userid, order.getAddress().getAddresstype(), order.getAddress().getAddress1(),
				order.getAddress().getAddress2(), order.getAddress().getCity(), order.getAddress().getState(),
				order.getAddress().getZip());
		Order ord = orderService.addToOrder(order, userid, address);

		return MapWebOrderBean(ord);
		
	}
	



	public WebOrder getOrderDetails(int orderid) {
		Order order = orderService.getOrderDetails(orderid);
		return MapWebOrderBean(order);
		
		
	}



	public WebOrder updateOrder(int orderid, int productid, int quantity) throws Exception {
		orderService.updateOrderQuantityForProduct(orderid, productid, quantity);
		return getOrderDetails(orderid);
	}



	public WebOrder cancelOrder(int orderid) {
		orderService.removeOrder(orderid);
		return getOrderDetails(orderid);
	}



	public WebOrder updateOrderShipping(int orderid, ShippingStatus status) {
		orderService.updateShippingDate(orderid, status.getShippingDate());
		orderService.updateShippingStatus(orderid, status.getShippingStatus());
		return getOrderDetails(orderid);
	}



	public WebOrder paymentProcessing(int orderid, Payment payment) {
		orderService.paymentProcessing(payment);
		return getOrderDetails(orderid);
	}
	
	
	public List<WebOrder> getAllOrderOfUserByStatus(int userid, String status) {
		List<Order> orderList =  orderService.getUserOrderDetailsByStatus(userid, status);
		List<WebOrder> webOrderList = new ArrayList<WebOrder>();
		
		for(Order ord : orderList)
		{
			webOrderList.add(MapWebOrderBean(ord));
		}
		
		return webOrderList;
		
		
	}
	
	public List<WebOrder> getAllOrderOfUser(int userid) {
		List<Order> orderList = orderService.getUserOrderDetails(userid);
		List<WebOrder> webOrderList = new ArrayList<WebOrder>();
		
		for(Order ord : orderList)
		{
			webOrderList.add(MapWebOrderBean(ord));
		}
		
		return webOrderList;
	}

}
