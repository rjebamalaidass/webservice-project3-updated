package edu.luc.webservices.activity;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.luc.webservices.bean.OrderOfPartner;
import edu.luc.webservices.domain.PartnerOrder;
import edu.luc.webservices.services.PartnerOrderService;
import edu.luc.webservices.services.PartnerService;

@Service
public class PartnerActivity {
	
	@Autowired
	PartnerOrderService partnerOrderService;
	
	public OrderOfPartner mapBeanObject(PartnerOrder o)
	{
		OrderOfPartner partner = new OrderOfPartner();
		partner.setProductid(o.getProductid());
		partner.setOrderid(o.getOrderid());
		partner.setProductname(o.getProductname());
		partner.setQuantity(o.getQuantity());
		partner.setDateorderreceived(o.getDateorderreceived());
		partner.setOrderfullfillmentstatus(o.getOrderfullfillmentstatus());
		partner.setDatefullfilled(o.getDatefullfilled());
		return partner;
	}

	public List<OrderOfPartner> getOrderList(int partnerid) {
		List<PartnerOrder> orderlist = partnerOrderService.getPartnerOrders(partnerid);
		List<OrderOfPartner> partnerList = new ArrayList<OrderOfPartner>();
		for(PartnerOrder o : orderlist)
		{
			
			partnerList.add(mapBeanObject(o));
		}
		
		return partnerList;
			
	}

	public OrderOfPartner updateOrderStatus(int partnerid, int orderid, OrderOfPartner orderOfPartner) {
		PartnerOrder order = partnerOrderService.updateOrderStatusAndDate(orderid, orderOfPartner.getOrderfullfillmentstatus(), orderOfPartner.getDatefullfilled());
		return mapBeanObject(order);
	}

	public List<OrderOfPartner> getPartnerOrderBystatus(int partnerid, String status) {
		List<OrderOfPartner> partnerOrderList = getOrderList(partnerid);
		partnerOrderList = partnerOrderList.stream().filter(order -> StringUtils.equalsIgnoreCase(status, order.getOrderfullfillmentstatus())).collect(Collectors.toList());
		return partnerOrderList;
	}

}
