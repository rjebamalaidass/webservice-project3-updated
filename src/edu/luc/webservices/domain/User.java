package edu.luc.webservices.domain;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="WEBSERVICES_USER")
public class User {
	
	@Id
	@SequenceGenerator(name = "SEQ_WEBSERVICES_CUSTOMER", sequenceName = "SEQ_WEBSERVICES_CUSTOMER", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_WEBSERVICES_CUSTOMER")
	@Column(name="USERID")
	private int userid;
	@Column(name="EMAIL")
	private String email;
	@Column(name="NAME")
	private String name;
	@Column(name="PHONE")
	private String phone;
	@Column(name="TYPE")
	private String type;
	
	@OneToMany
	@JoinColumn(name="USERID" )
	@JsonIgnore
	private List<Address> address;

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<Address> getAddress() {
		return address;
	}

	public void setAddress(List<Address> address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "User [userid=" + userid + ", email=" + email + ", name=" + name + ", phone=" + phone + ", type=" + type
				+ ", address=" + address + "]";
	}

	
	
	
	

}
