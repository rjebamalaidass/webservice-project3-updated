package edu.luc.webservices.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="WEBSERVICES_LOGIN")
public class Login {
	
	@Id
	@Column(name="CUSTOMERID")
	private int customerid;
	
	@Column(name="USERNAME")
	private String username;
	
	@Column(name="password")
	private String password;

	public int getCustomerid() {
		return customerid;
	}

	public void setCustomerid(int customerid) {
		this.customerid = customerid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
	
	

}
