package edu.luc.webservices.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name="WEBSERVICES_ORDER_DETAIL")
@IdClass(OrderDetailIDClass.class)
public class OrderDetail {
	
	@Id
	@Column(name="ORDERID")
	private int orderid;
	
	@Id
	@Column(name="PRODUCTID")
	private int productid;
	
	
	@Column(name="QUANTITY")
	private int quantity;
	
	
	@Column(name="TOTALPRICE")
	private double totalprice;
	
	@OneToOne
	@JoinColumn(name="ORDERID", insertable = false, updatable = false)
	private Order order;
	
	@OneToOne
	@JoinColumn(name="PRODUCTID", insertable = false, updatable = false)
	private Product product;

	public int getOrderid() {
		return orderid;
	}


	public void setOrderid(int orderid) {
		this.orderid = orderid;
	}


	public int getProductid() {
		return productid;
	}


	public void setProductid(int productid) {
		this.productid = productid;
	}


	public int getQuantity() {
		return quantity;
	}


	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}


	public double getTotalprice() {
		return totalprice;
	}


	public void setTotalprice(double totalprice) {
		this.totalprice = totalprice;
	}


	public Order getOrder() {
		return order;
	}


	public void setOrder(Order order) {
		this.order = order;
	}


	public Product getProduct() {
		return product;
	}


	public void setProduct(Product product) {
		this.product = product;
	}


	@Override
	public String toString() {
		return "OrderDetail [orderid=" + orderid + ", productid=" + productid + ", quantity=" + quantity
				+ ", totalprice=" + totalprice + ", order=" + order.toString() + ", product=" + product.toString() + "]";
	}
	
	
	
	

}
