package edu.luc.webservices.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="WEBSERVICES_REVIEW")
public class Review {
	
	@Id
	@SequenceGenerator(name = "SEQ_WEBSERVICES_REVIEW", sequenceName = "SEQ_WEBSERVICES_REVIEW", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_WEBSERVICES_REVIEW")
	@Column(name="REVIEWID")
	private int reviewid;
	
	@Column(name="PRODUCTID")
	private int productid;
	
	@Column(name="USERID")
	private int userid;
	
	@Column(name="DESCRIPTION")
	private String description;
	
	@Column(name="RATING")
	private int rating;
	
	@OneToOne
	@JoinColumn(name="PRODUCTID", insertable = false, updatable = false)
	private Product product;
	
	@OneToOne
	@JoinColumn(name="USERID", insertable = false, updatable = false)
	private User user;

	public int getReviewid() {
		return reviewid;
	}

	public void setReviewid(int reviewid) {
		this.reviewid = reviewid;
	}

	public int getProductid() {
		return productid;
	}

	public void setProductid(int productid) {
		this.productid = productid;
	}

	

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "Review [reviewid=" + reviewid + ", productid=" + productid + ", userid=" + userid + ", description="
				+ description + ", rating=" + rating + ", product=" + product + ", user=" + user + "]";
	}

	
	

}
