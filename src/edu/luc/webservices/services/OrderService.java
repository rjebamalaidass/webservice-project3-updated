package edu.luc.webservices.services;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.luc.webservices.bean.Payment;
import edu.luc.webservices.bean.ProductOrder;
import edu.luc.webservices.bean.WebOrder;
import edu.luc.webservices.data.OrderRepository;
import edu.luc.webservices.domain.Order;
import edu.luc.webservices.domain.OrderDetail;
import edu.luc.webservices.domain.Product;

@Service
public class OrderService {
	
	@Autowired
	ProductService productService;
	
	@Autowired
	OrderRepository orderRepository;
	
	@Autowired
	OrderDetailService orderDetailService;
	
	@Autowired
	PartnerOrderService partnerOrderService;
	
	
	public Order getOrderDetails(Integer orderid)
	{
		return orderRepository.findById(orderid).get();
	}
	
	/**
	 * When newOrUpdate value is true, that means it is a new entry in the order and the order detail is newly created
	 */	
	public double createOrUpdateOrderQuantity(int orderid, int productId, int quantity, boolean newOrUpdate) throws Exception
	{
		double totalPrice = 0;
		Product product = productService.getProductById(productId);
		
		/**
		 * Check if the ordered amoutn of the product is already available in the inventory
		 * if yes, we set in the cart or throw an exception as product not available
		 */
		if(product.getAvailablequantity()>=quantity)
		{
			double price = product.getPrice();
			
			totalPrice = price*quantity;
			/**
			 * When newOrUpdate value is true, that means it is a new entry in the order and the order detail is newly created
			 */
			if(newOrUpdate)
			{
				orderDetailService.insertOrderDetail(orderid, productId, quantity, price*quantity);
			}
			else
			{
				orderDetailService.updateQuantity(orderid, productId, quantity, totalPrice);
			}
			return totalPrice;
		}
		else
		{
			throw new Exception("Product Not Available in Inventory");
		}
	}
	
	public Order addToOrder(WebOrder ord, int userid, int addressid) throws Exception
	{
		Order order = new Order();
		order.setAddressid(addressid);
		order.setDatereceived(new Date());
		order.setOrderdate(new Date());
		order.setOrderstatus("PAYMENT_PENDING");
		order.setPaymentstatus("PENDING");
		order.setShippingcost(5.00);
		order.setUserid(userid);
		order.setActive("y");
		
		order = orderRepository.saveAndFlush(order);
		
		double totalPrice = 0.0;
		List<ProductOrder> productList = ord.getProductList();
		for(ProductOrder prod : productList)
		{
				Product product = productService.getProductById(prod.getProductId());
				int quantity = prod.getQuantity();
				
				totalPrice = totalPrice + createOrUpdateOrderQuantity(order.getOrderid(), product.getProductid(), quantity, true);
				
				
		}
		
		order.setOrderprice(totalPrice);
		order.setTotalprice(order.getOrderprice()+order.getShippingcost());
		order = orderRepository.saveAndFlush(order);
		
		return order;
		
		
	}
	
	public int insertOrder(int userid, double d, int addressid, double e, double f, Date orderdate)
	{
		Order order = new Order();
		order.setUserid(userid);
		order.setAddressid(addressid);
		order.setOrderdate(orderdate);
		order.setOrderprice(d);
		order.setShippingcost(e);
		order.setTotalprice(f);
		order.setActive("y");
		
		order = orderRepository.saveAndFlush(order);
		
		return order.getOrderid();
	}
	
	public boolean updatePaymentStatus(int orderid, String paymentstatus, String confirmationnumber)
	{
		Order order = orderRepository.getOne(orderid);
		
		order.setPaymentstatus(paymentstatus);
		order.setPaymentconfirmation(confirmationnumber);
		
		orderRepository.saveAndFlush(order);
		
		return true;
		
		
	}
	
	public boolean updateShippingDate(int orderid, Date shippingDate)
	{
		Order order = orderRepository.getOne(orderid);
		order.setShippingdate(shippingDate);
		orderRepository.saveAndFlush(order);
		
		return true;
	}
	

	public boolean updateShippingStatus(int orderid, String shippingstatus)
	{
		Order order = orderRepository.getOne(orderid);
		order.setShippingstatus(shippingstatus);
		orderRepository.saveAndFlush(order);
		
		return true;
	}
	
	public boolean updateOrderStatus(int orderid, String orderStatus)
	{
		Order order = orderRepository.getOne(orderid);
		order.setOrderstatus(orderStatus);
		orderRepository.saveAndFlush(order);
		
		return true;
	}
	
	private String processPayment(Payment payment)
	{
		/**
		 * This part will sent payment detail to third party payment processor and will get
		 * Payment confirmation
		 * the following line creates dummy payment confirmation number
		 */
		
		UUID uuid = UUID.randomUUID();
		return uuid.toString();
	}
//
	public String paymentProcessing(Payment payment) {
		
		String paymentConfirmation = processPayment(payment);
		Order order = orderRepository.findById(Integer.getInteger(payment.getOrderid())).get();
		
		order.setOrderdate(new Date());
		order.setOrderstatus("PAYMENT_PROCESSED - ORDER_INITIATED");
		order.setPaymentconfirmation(paymentConfirmation);
		order.setPaymentstatus("SUCCESS");
		order.setShippingstatus("PENDING");
		orderRepository.saveAndFlush(order);
		
		
		List<OrderDetail> orderDetailList = orderDetailService.findByOrderId(order.getOrderid());
		orderDetailList.stream().forEach(orderdetail ->
		{
			int productid = orderdetail.getProductid();
			Product product = productService.getProductById(Integer.valueOf(productid));
			int partnerid = product.getPartnerid();
			/*
			 * We find the partner id for each product. if the partner id is greater than zero
			 * then the product will be processed and shipped by the partner.
			 * So the partner will be notified about this order by updating the partner order table.
			 * Partner will call seperate webservice to pull in their order details
			 */
			if(partnerid>0)
			{
				partnerOrderService.insertPartnerOrder(partnerid, order.getOrderid(), new Date(), orderdetail.getQuantity(), productid, product.getProductname());
			}
		}
				);
		
		return paymentConfirmation;
		
		
		
	}

	public void saveOrder(Order order) {
		orderRepository.saveAndFlush(order);
		
		
	}

	public Boolean updateOrderQuantityForProduct(int orderId, int productId, int quantity)  throws Exception {
		
		createOrUpdateOrderQuantity(orderId, productId, quantity, false);
		List<OrderDetail> orderDetailList = orderDetailService.findByOrderId(orderId);
		
		/***
		 * Updating the total Price as the total price of the order will be changed due to change in the quantity
		 */
		double totalPrice = 0.0;
		for(OrderDetail detail : orderDetailList)
		{
			totalPrice = totalPrice + detail.getTotalprice();
		}
		
		Order order = orderRepository.findById(orderId).get();
		order.setTotalprice(totalPrice);
		orderRepository.saveAndFlush(order);
		
		return true;
		
	}

	public void removeOrder(int orderid) {
		orderDetailService.deleteByOrderId(orderid);
		Order order = orderRepository.findById(orderid).get();
		order.setActive("n");
		order.setRefundstatus("REFUND - INITIATED");
		order.setCancelledDate(new Date());
		orderRepository.saveAndFlush(order);
		
	}

	public List<Order> getUserOrderDetails(int userid) {
		return orderRepository.findByUserid(userid);
	}

	public List<Order> getUserOrderDetailsByStatus(int userid, String status) {
		return orderRepository.findByUseridAndOrderstatus(userid, status);
	}

	
	
	

}
