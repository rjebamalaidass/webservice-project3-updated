package edu.luc.webservices.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.luc.webservices.data.OrderDetailRepository;
import edu.luc.webservices.domain.OrderDetail;
import edu.luc.webservices.domain.OrderDetailIDClass;

@Service
public class OrderDetailService {
	
	@Autowired
	OrderDetailRepository orderDetailRepository;
	
	public void insertOrderDetail(int orderid, int productid, int quantity, double price)
	{
		OrderDetail detail = new OrderDetail();
		detail.setOrderid(orderid);
		detail.setProductid(productid);
		detail.setQuantity(quantity);
		detail.setTotalprice(price);
		
		orderDetailRepository.saveAndFlush(detail);
	} 
	
	public void updateQuantity(int orderid, int productid, int quantity, double price)
	{
		OrderDetailIDClass idClass = new OrderDetailIDClass();
		idClass.setOrderid(orderid);
		idClass.setProductid(productid);
		OrderDetail detail = orderDetailRepository.getOne(idClass);
		if(quantity>0)
		{
			detail.setQuantity(quantity);
			detail.setTotalprice(price);
			orderDetailRepository.saveAndFlush(detail);
		}
		else
		{
			orderDetailRepository.deleteById(idClass);
		}
	}
	
	public void deleteProductFromTheOrder(int orderid, int productid)
	{
		OrderDetailIDClass idClass = new OrderDetailIDClass();
		idClass.setOrderid(orderid);
		idClass.setProductid(productid);
		OrderDetail detail = orderDetailRepository.getOne(idClass);
		orderDetailRepository.delete(detail);
	}
	
	public void deleteByOrderId(int orderid)
	{
		orderDetailRepository.deleteByOrderid(orderid);
	}

	public List<OrderDetail> findByOrderId(int orderid) {
		return orderDetailRepository.findByOrderid(Integer.valueOf(orderid));
	}

}
