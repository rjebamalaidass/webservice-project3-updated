package edu.luc.webservices.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.luc.webservices.data.AddressRepository;
import edu.luc.webservices.data.PartnerRepository;
import edu.luc.webservices.domain.Address;
import edu.luc.webservices.domain.Order;
import edu.luc.webservices.domain.Partner;

@Service
public class PartnerService {
	
	@Autowired
	PartnerRepository partnerRepository;
	
	@Autowired
	AddressRepository addressRepository;
	
	
	public int insertPartner(String name, String emailaddress, String phone, String addresstype, String address1, String address2, String city, String state, String zip)
	{
		Address address = new Address();
		address.setAddresstype(addresstype);
		address.setAddress1(address1);
		address.setAddress2(address2);
		address.setCity(city);
		address.setState(state);
		address.setZip(zip);
		//address.setCustomerid(partner.getPartnerid());
		
		addressRepository.saveAndFlush(address);
		
		Partner partner = new Partner();
		partner.setPartnername(name);
		partner.setEmailaddress(emailaddress);
		partner.setPhone(phone);
		partner.setAddressid(address.getAddressid());
		partner = partnerRepository.saveAndFlush(partner);
		
		address.setUserid(partner.getPartnerid());
		addressRepository.saveAndFlush(address);
		
		
		
		return partner.getPartnerid();
	}
	
	public void updatePartnerDetails(int partnerId, String name, String emailaddress, String phone )
	{
		Partner partner = partnerRepository.getOne(partnerId);
		partner.setPartnername(name);
		partner.setEmailaddress(emailaddress);
		partner.setPhone(phone);
		partnerRepository.saveAndFlush(partner);
	}

	

}
