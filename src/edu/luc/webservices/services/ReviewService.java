package edu.luc.webservices.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.luc.webservices.data.ReviewRepository;
import edu.luc.webservices.domain.Review;

@Service
public class ReviewService {
	
	@Autowired 
	ReviewRepository reviewRepository;
	
	public void insertReview(int productid, int userid, String description, int rating)
	{
		Review review = new Review();
		review.setUserid(userid);
		review.setProductid(productid);
		review.setDescription(description);
		review.setRating(rating);
		reviewRepository.saveAndFlush(review);
	}
	
	

}
