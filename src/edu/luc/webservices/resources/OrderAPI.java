package edu.luc.webservices.resources;

import javax.jws.WebService;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import edu.luc.webservices.bean.Payment;
import edu.luc.webservices.bean.ShippingStatus;
import edu.luc.webservices.bean.WebOrder;

@Path("/order")
@WebService
public interface OrderAPI {
	
    @POST
    @Path("placeorder/{userid}")
    @Produces(MediaType.APPLICATION_JSON)
    WebOrder placeOrder(@PathParam("userid") int userid, WebOrder order) throws Exception;

    @GET
    @Path("orderDetail/{orderid}")
    @Produces(MediaType.APPLICATION_JSON)
	WebOrder getOrder(@PathParam("orderid") int orderid);
    
	@DELETE
	@Path("removeFromOrder/{orderid}/{productid}")
	@Produces(MediaType.APPLICATION_JSON)
	WebOrder removeProductFromCart(@PathParam("orderid") int orderid, @PathParam("productid") int productid) throws Exception;
  

	@DELETE
	@Path("cancelOrder/{orderid}")
	@Produces(MediaType.APPLICATION_JSON)
	WebOrder cancelOrder(@PathParam("orderid") int orderid);
	
	
    @POST
    @Path("paymentdetail/{orderid}")
    @Produces(MediaType.APPLICATION_JSON)
    WebOrder paymentProcessing(@PathParam("orderid") int orderid, Payment payment) throws Exception;
    
    
    @POST
    @Path("updateOrder/{orderid}/{productid}")
    @Produces(MediaType.APPLICATION_JSON)
    WebOrder updateOrderQuantity(@PathParam("orderid") int orderid, @PathParam("productid") int productid, int quantity) throws Exception;
    
    
    @POST
    @Path("updateOrderShipping/{orderid}/")
    @Produces(MediaType.APPLICATION_JSON)
    WebOrder updateOrderShipping(@PathParam("orderid") int orderid, ShippingStatus status) throws Exception;
  
    



}
