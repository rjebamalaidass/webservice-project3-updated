package edu.luc.webservices.resources;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import edu.luc.webservices.bean.WebOrder;
import edu.luc.webservices.activity.ProductActivity;
import edu.luc.webservices.bean.Login;
import edu.luc.webservices.bean.OrderOfPartner;
import edu.luc.webservices.bean.Payment;
import edu.luc.webservices.bean.ProductBean;
import edu.luc.webservices.bean.RegisterDetail;
import edu.luc.webservices.bean.WebAddress;
import edu.luc.webservices.services.AddressService;
import edu.luc.webservices.services.LoginService;
import edu.luc.webservices.services.OrderDetailService;
import edu.luc.webservices.services.OrderService;
import edu.luc.webservices.services.PartnerOrderService;
import edu.luc.webservices.services.PartnerService;
import edu.luc.webservices.services.ProductService;
import edu.luc.webservices.services.UserService;


@Service
public class ProductController implements ProductAPI {

	@Autowired
	ProductActivity productActivity;
	
//	@Autowired
//	UserService userService;
//	
//	@Autowired
//	LoginService loginService;
//	
//	@Autowired
//	OrderService orderService;
//	
//	@Autowired
//	AddressService addressService;
//	
//	@Autowired
//	OrderDetailService orderDetailService;
//	
//	@Autowired
//	PartnerOrderService partnerOrderService;


	
//	@Override
//	public User register(RegisterDetail register) {
//		return userService.registerUser(register);
//	}
//
//
//	@Override
//	public User setupLogin(Login login) throws Exception {
//		try {
//			return loginService.setupLoginCredentials(login.getUserid(), login.getUserName(), login.getPassword());
//		} catch (Exception e) {
//			
//			e.printStackTrace();
//			throw e;
//		}
//	}
//
//
//	@Override
//	public User login(Login login) {
//		return loginService.authenticate(login.getUserName(), login.getPassword());
//	}

	@Override

	public List<ProductBean> getProductList()
	{
		return productActivity.getAllProducts();
		
	}

	@Override
	public ProductBean getProductDetails(int productId) {
		return productActivity.getProductById(productId);
	}
	
	@Override
	public List<String> getCategories() {
		return productActivity.getAllProductCategories();
	}


	@Override
	public List<ProductBean> getProductByCategory(String a) {
		return productActivity.getProductByCategory(a);
	}
	
	@Override
	public ProductBean addPartnerProduct(int partnerid, ProductBean product) {
		
		return productActivity.insertProduct(product.getProductname(), product.getDescription(), product.getCategory(), partnerid);
		
	}
	
	@Override
	public ProductBean updateProductDetail(ProductBean product)
	{
		return productActivity.updateProductDetail(product);
	}
	
//	@Override 
//	public ProductBean addProductQuantity(int productid, int quantity)
//	{
//		ProductBean productActivity.updateQuantity(productid, quantity);
//		
//		
//	}
//	@Override 
//	public ProductBean addProductprice(int productid, double price)
//	{
//		return productActivity.updatePrice(productid, price);
//		
//		
//	}
//	@Override 
//	public ProductBean updateProductName(int productid, String name)
//	{
//		return productActivity.updateName(productid, name);
//		
//		
//	}
//	@Override 
//	public ProductBean updateProductDescription(int productid, String description)
//	{
//		return productActivity.updateDescription(productid, description);
//		
//		
//	}
//
//
//	@Override
//	public ProductBean getAvailableProductQuantity(int productid) {
//		return productActivity.getProductById(productid).getAvailablequantity();
//	}
//	
//	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
//
//	@Override
//	public WebOrder addToOrder(int userid, int addressid, WebOrder order) throws Exception {
//		return orderService.addToOrder(order, Integer.valueOf(userid), Integer.valueOf(addressid));
//	}
//
//
//	@Override
//	public int addAddress(WebAddress address) {
//		return addressService.insertAddress(address.getUserid(), address.getAddresstype(), address.getAddress1(),
//				address.getAddress2(), address.getCity(), address.getState(), address.getZip());
//		
//	}
//
//
//	@Override
//	public String paymentProcessing(Payment payment) {
//		return orderService.paymentProcessing(payment);
//	}
//
//
//	@Override
//	public Boolean updateShippingAddress(int orderid, WebAddress address) {
//		return addressService.updateShippingAddress(orderid, address);
//	}
//
//
//	@Override
//	public Boolean updateOrderQuantity(int orderid, int productid, int quantity) throws Exception {
//		return orderService.updateOrderQuantityForProduct(orderid, productid, quantity);
//	}
//
//
//	@Override
//	public Boolean removeProductFromCart(int orderid, int productid) {
//		orderDetailService.deleteProductFromTheOrder(orderid, productid);
//		return true;
//	}
//
//
//	@Override
//	public Boolean cancelOrder(int orderid) {
//		orderService.removeOrder(orderid);
//		return true;
//	}
//
//
//	@Override
//	public WebOrder getOrder(int orderid) {
//		return orderService.getOrderDetails(orderid);
//	}
//
//
//	@Override
//	public List<WebOrder> getAllOrderOfUser(int userid) {
//		return orderService.getUserOrderDetails(userid);
//	}
//
//

//
//
//	@Override
//	public List<WebOrder> getAllOrderOfUserByStatus(int userid, String status) {
//		return orderService.getUserOrderDetailsByStatus(userid, status);
//	}
//
//
//	@Override
//	public List<OrderOfPartner> getAllOrderOfPartner(int partnerid) {
//		List<PartnerOrder> partnerOrderList =  partnerOrderService.getPartnerOrders(partnerid);
//		
//		List<OrderOfPartner> partnerList = new ArrayList<OrderOfPartner>();
//		for(PartnerOrder partnerOrder : partnerOrderList)
//		{
//			OrderOfPartner partner = new OrderOfPartner();
//			partner.setDatefullfilled(partnerOrder.getDatefullfilled());
//			partner.setDateorderreceived(partnerOrder.getDateorderreceived());
//			partner.setOrderfullfillmentstatus(partnerOrder.getOrderfullfillmentstatus());
//			
//			int orderid = partnerOrder.getOrderid();
//			List<OrderDetail> orderDetailList = orderDetailService.findByOrderId(orderid);
//			for(OrderDetail detail : orderDetailList)
//			{
//				int product_partner_id = detail.getProduct().getPartnerid();
//				if(product_partner_id == partnerid)
//				{
//					partner.setProductid(detail.getProductid());
//					partner.setProductname(detail.getProduct().getProductname());
//					partner.setQuantity(detail.getQuantity());
//				}
//			}
//					
//			partnerList.add(partner);
//		}
//			return partnerList;
//		
//	}
//
//
//	@Override
//	public List<OrderOfPartner> getPartnerOrderByStatus(int partnerid, String status) {
//		List<OrderOfPartner> partnerOrderList = getAllOrderOfPartner(partnerid);
//		partnerOrderList = partnerOrderList.stream().filter(order -> StringUtils.equalsIgnoreCase(status, order.getOrderfullfillmentstatus())).collect(Collectors.toList());
//		return partnerOrderList;
//		
//	}
//
//
//	@Override
//	public Boolean updateOrderShippingStatus(int orderid, String shippingstatus) {
//		orderService.updateShippingStatus(orderid, shippingstatus);
//		orderService.updateShippingDate(orderid, new Date());
//		return true;
//	}
//
//

//
//





}
