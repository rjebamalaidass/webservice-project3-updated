package edu.luc.webservices.resources;

import java.util.List;

import javax.jws.WebService;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.stereotype.Service;


import edu.luc.webservices.bean.WebOrder;
import edu.luc.webservices.bean.Login;
import edu.luc.webservices.bean.OrderOfPartner;
import edu.luc.webservices.bean.Payment;
import edu.luc.webservices.bean.ProductBean;
import edu.luc.webservices.bean.RegisterDetail;
import edu.luc.webservices.bean.WebAddress;


@Path("/product")
@WebService
public interface ProductAPI {


//	 @POST
//	 @Path("/register")
//	 @Produces(MediaType.APPLICATION_JSON)
//	 User register(RegisterDetail register);
//	 
//	@POST
//	@Path("/setuplogin")
//	@Produces(MediaType.APPLICATION_JSON)
//	User setupLogin(Login login) throws Exception;
//    
//    @POST
//    @Path("/login")
//    @Produces(MediaType.APPLICATION_JSON)
//    User login(Login login);
      
    
    @GET
    @Path("/productlist")
    @Produces(MediaType.APPLICATION_JSON)
    List<ProductBean> getProductList();
    
    @GET
    @Path("category")
    @Produces(MediaType.APPLICATION_JSON)
    List<String> getCategories();
    
    @GET
    @Path("productbycategory/{category}")
    @Produces(MediaType.APPLICATION_JSON)
    List<ProductBean> getProductByCategory(@PathParam("category") String a);
    
    
    @GET
    @Path("productdetails/{productid}")
    @Produces(MediaType.APPLICATION_JSON)
    ProductBean getProductDetails(@PathParam("productid") int productid);
    
    @POST
    @Path("addProduct/{partnerid}")
    @Produces(MediaType.APPLICATION_JSON)
    ProductBean addPartnerProduct(@PathParam("partnerid") int partnerid, ProductBean product);
    
    @POST
    @Path("updateProduct")
    @Produces(MediaType.APPLICATION_JSON)
    ProductBean updateProductDetail(ProductBean product);
    

    
//    @GET
//    @Path("getAvailableProductQuantity/{productid}")
//    @Produces(MediaType.APPLICATION_JSON)
//    ProductBean getAvailableProductQuantity(@PathParam("productid") int productid);
//
//    @POST
//    @Path("updateProductDescription/{productid}")
//    @Produces(MediaType.APPLICATION_JSON)
//    ProductBean updateProductDescription(@PathParam("productid") int productid, String description);
//
//    @POST
//    @Path("updateProductName/{productid}")
//    @Produces(MediaType.APPLICATION_JSON)
//    ProductBean updateProductName(@PathParam("productid") int productid, String name);
//
//    @POST
//    @Path("updateProductprice/{productid}")
//    @Produces(MediaType.APPLICATION_JSON)    
//    ProductBean addProductprice(@PathParam("productid") int productid, double price);
//
//    @POST
//    @Path("updateProductquantity/{productid}")
//    @Produces(MediaType.APPLICATION_JSON)
//    ProductBean addProductQuantity(@PathParam("productid") int productid, int quantity);
////    
//    @POST
//    @Path("addAddress")
//    int addAddress(WebAddress address);
//    
//    @POST
//    @Path("addtoorder/{userid}/{addressid}")
//    @Produces(MediaType.APPLICATION_JSON)
//   	WebOrder addToCart(@PathParam("userid") int userid, @PathParam("addressid") int addressid, WebOrder order) throws Exception;
//    
//    @POST
//    @Path("payment")
//    @Produces(MediaType.APPLICATION_JSON)
//   	String paymentProcessing(Payment payment);
//    
//    @POST
//    @Path("shippingAddress/{orderid}")
//    @Produces(MediaType.APPLICATION_JSON)
//   	Boolean updateShippingAddress(@PathParam("orderid") int orderid, WebAddress address);
//  
//    @POST
//    @Path("updateOrder/{orderid}/{productid}")
//    @Produces(MediaType.APPLICATION_JSON)
//    Boolean updateOrderQuantity(@PathParam("orderid") int orderid, @PathParam("productid") int productid, int quantity) throws Exception;
//    
//    @DELETE
//    @Path("removeFromOrder/{orderid}/{productid}")
//    @Produces(MediaType.APPLICATION_JSON)
//    Boolean removeProductFromCart(@PathParam("orderid") int orderid, @PathParam("productid") int productid);
//
//    @DELETE
//    @Path("cancelOrder/{orderid}")
//    @Produces(MediaType.APPLICATION_JSON)
//    Boolean cancelOrder(@PathParam("orderid") int orderid);
//    
//    @GET
//    @Path("orderdetails/{orderid}")
//    @Produces(MediaType.APPLICATION_JSON)
//    WebOrder getOrder(@PathParam("orderid") int orderid);
//    
//    @GET
//    @Path("orderdetails/{userid}")
//    @Produces(MediaType.APPLICATION_JSON)
//    List<WebOrder> getAllOrderOfUser(@PathParam("userid") int userid);
//    
//    @GET
//    @Path("orderdetails/{userid}/{status}")
//    @Produces(MediaType.APPLICATION_JSON)
//    List<WebOrder> getAllOrderOfUserByStatus(@PathParam("userid") int userid, @PathParam("status") String status);
//    
//    @GET
//    @Path("orderdetails/{partnerid}")
//    @Produces(MediaType.APPLICATION_JSON)
//    List<OrderOfPartner> getAllOrderOfPartner(@PathParam("partnerid") int partnerid);
//    
//    @GET
//    @Path("orderdetails/{partnerid}/{status}")
//    @Produces(MediaType.APPLICATION_JSON)
//    List<OrderOfPartner> getPartnerOrderByStatus(@PathParam("partnerid") int partnerid, @PathParam("status") String status);
//    
//    
//    
//    
//    @POST
//    @Path("updateOrderShippingStatus/{orderid}")
//    @Produces(MediaType.APPLICATION_JSON)
//    Boolean updateOrderShippingStatus(@PathParam("orderid") int orderid, String shippingstatus);
//    
//    

//    
//    
//    
//    
    


}