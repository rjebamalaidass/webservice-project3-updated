package edu.luc.webservices.resources;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.luc.webservices.activity.OrderActivity;
import edu.luc.webservices.activity.PartnerActivity;
import edu.luc.webservices.bean.OrderOfPartner;
import edu.luc.webservices.bean.WebOrder;

@Service
public class ReportController implements ReportAPI {
	
	@Autowired
	OrderActivity orderActivity;
	
	@Autowired
	PartnerActivity partnerActivity;

	@Override
	public List<WebOrder> getAllOrderOfUser(int userid) {
		return orderActivity.getAllOrderOfUser(userid);
	}

	@Override
	public List<WebOrder> getAllOrderOfUserByStatus(int userid, String status) {
		return orderActivity.getAllOrderOfUserByStatus(userid, status);
	}

	@Override
	public List<OrderOfPartner> getAllOrderOfPartner(int partnerid) {
		return partnerActivity.getOrderList(partnerid);
	}

	@Override
	public List<OrderOfPartner> getPartnerOrderByStatus(int partnerid, String status) {
		return partnerActivity.getPartnerOrderBystatus(partnerid, status);
	}
	
	

}
