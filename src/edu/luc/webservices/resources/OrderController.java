package edu.luc.webservices.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.luc.webservices.activity.OrderActivity;
import edu.luc.webservices.bean.Payment;
import edu.luc.webservices.bean.ProductBean;
import edu.luc.webservices.bean.ShippingStatus;
import edu.luc.webservices.bean.WebOrder;

@Service
public class OrderController implements OrderAPI {
	
	@Autowired
	OrderActivity orderActivity;

	@Override
	public WebOrder placeOrder(int userid, WebOrder order) throws Exception {
		return orderActivity.placeOrder(userid, order);
	}
	

	@Override
	public WebOrder paymentProcessing(int orderid, Payment payment) throws Exception {
		return orderActivity.paymentProcessing(orderid, payment);
	}
	
	@Override
	public WebOrder getOrder(int orderid) {
		return orderActivity.getOrderDetails(orderid);
	}

	@Override
	public WebOrder removeProductFromCart(int orderid, int productid) throws Exception {
		return orderActivity.updateOrder(orderid, productid, 0);
	}

	@Override
	public WebOrder cancelOrder(int orderid) {
		return orderActivity.cancelOrder(orderid);
	}

	@Override
	public WebOrder updateOrderQuantity(int orderid, int productid, int quantity) throws Exception {
		return orderActivity.updateOrder(orderid, productid, quantity);
	}

	@Override
	public WebOrder updateOrderShipping(int orderid, ShippingStatus status) throws Exception {
		return orderActivity.updateOrderShipping(orderid, status);
	}

}
