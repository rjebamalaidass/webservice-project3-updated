package edu.luc.webservices.resources;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.luc.webservices.activity.PartnerActivity;
import edu.luc.webservices.bean.OrderOfPartner;

@Service
public class PartnerController implements PartnerAPI {
	
	@Autowired
	PartnerActivity partnerActivity;

	@Override
	public List<OrderOfPartner> getOrderList(int partnerid) throws Exception {
		return partnerActivity.getOrderList(partnerid);
	}

	@Override
	public OrderOfPartner updateOrderstatus(int partnerid, int orderid, OrderOfPartner orderOfPartner) {
		return partnerActivity.updateOrderStatus(partnerid, orderid, orderOfPartner);
	}

}
